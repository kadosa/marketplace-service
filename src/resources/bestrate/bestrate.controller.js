'use strict';

var products = require('../products/products.json');

exports.index = function*(next) {
	if (!products || products.length === 0) {
		this.status = 404;
		this.body = 'No products found';
	} else {
		console.log('i am still running why');
		this.status = 200;

		// sort by rate desc and get the first items rate attribute
		const bestRate = products.sort(function(a, b) {
			return b.rate - a.rate;
		})[0].rate;

		this.body = bestRate;
	}

};
