'use strict';


var app = require('../../server');
var request = require('supertest').agent(app.listen());

var expect = require('chai').expect;
var should = require('should');


describe('GET /bestrate', function(){
  it('should respond with 200 type Array', function(done){
    request
    .get('/bestrate')
    .expect(200, function(err, res) {
    	expect(res.body).to.be.a('number');
    	done();
    });
  });
});
